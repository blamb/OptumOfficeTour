﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GazeSelect : MonoBehaviour {

	public Transform LoadingBar;
	public Transform TextIndicator;
	public Transform TextLoading;
	[SerializeField] private float currentAmount;
	[SerializeField] public float speed;
	
	// Update is called once per frame
	void Update () {
		if (currentAmount < 100) {
			currentAmount += speed * Time.deltaTime;
			TextIndicator.GetComponent<Text> ().text = ((int)currentAmount).ToString () + "%";
			TextLoading.gameObject.SetActive (true);
		}

		else{
			TextLoading.gameObject.SetActive(false);
			TextIndicator.GetComponent<Text>().text="done";
		}

		if (currentAmount < 0) {
			speed = 0;

		}
		LoadingBar.GetComponent<Image> ().fillAmount = currentAmount / 100;
	}

	public void UpdateSpeed(float newSpeed ){
		speed = newSpeed;
	}
}