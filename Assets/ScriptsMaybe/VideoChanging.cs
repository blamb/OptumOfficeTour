﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoChanging : MonoBehaviour {

	public VideoPlayer playerVid;


	// Use this for initialization
	void Start () {
		//get component call

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void switchVideos(VideoClip video){
		playerVid.Stop ();
		playerVid.clip = video;
		playerVid.Play ();
	}
}
