﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Combined : MonoBehaviour {

	public VideoPlayer playerVid;
	public VideoClip nextClip;

	public Transform LoadingBar;
	public Transform TextIndicator;
	public Transform TextLoading;

	[SerializeField] private float currentAmount;
	[SerializeField] public float speed;

	// Update is called once per frame
	void Update () {

		//User is looking at the menu, waiting for next video to load
		if (currentAmount < 100) {
			currentAmount += speed * Time.deltaTime;
			TextIndicator.GetComponent<Text> ().text = ((int)currentAmount).ToString () + "%";
			TextLoading.gameObject.SetActive (true);

		}

		//loading bar has completed 
		else{
			TextLoading.gameObject.SetActive(false);
			TextIndicator.GetComponent<Text>().text="done";
			switchVideos (nextClip);
			speed = 0;
			currentAmount = 0;
		}

		//User isnt looking at the UX
		if (currentAmount < 0) {
			speed = 0;

		}
		LoadingBar.GetComponent<Image> ().fillAmount = currentAmount / 100;

	}

	//User has entered or exited a button
	public void UpdateSpeed(float newSpeed){
		speed = newSpeed;
	}
	public void UpdateCurrentAmount(float newCurrentAmount){
		currentAmount = newCurrentAmount;
	}

	public void UpdateNextVideo(VideoClip nClip){
		//update the next clip to what they are looking at
		nextClip = nClip;
	}

	public void switchVideos(VideoClip video){
		playerVid.Stop ();
		playerVid.clip = video;
		playerVid.Play ();
	}
}